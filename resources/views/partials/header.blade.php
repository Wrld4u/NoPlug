<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        {{--<div class="logo">--}}
            {{--<img style="width:75px;" src="/img/logo.png">--}}
            {{--<a class="navbar-brand ml-2" href="{{ url('/') }}" style="height: 100%;"><span style="font-family: 'trebuchet ms', geneva, 'helvetica cy', sans-serif; color: #c53a58; font-size: 1.7rem;"><strong>ОРБИТА+</strong></span></a>--}}
        {{--</div>--}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                {{--menu--}}
                {{--<li class="wide-bold">--}}
                {{--<a href="tel:+74732592181">тел: +7(473)259-21-81</a>--}}
                {{--<p class="m-0">Воронеж, Северный район, Лизюкова 56</p>--}}
                {{--</li>--}}
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('msg.Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('msg.Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('home') }}">Личный кабинет</a>
                            <?php
                            $role = auth()->user();
                            if ($role->role == 1) {
                            ?>
                            <a class="dropdown-item" href="{{ route('admin.index') }}">Админский кабинет</a>
                            <?php
                            }
                            ?>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('msg.Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
        {{--<div class="sidenav-icon"><a href="#"><i class="fa fa-shopping-basket"></i></a></div>--}}
    </div>
</nav>
