<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Сравни тексты</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            p {
                font-size: 0.9rem;
                margin-bottom: 25px;
            }

            pre {
                margin: 10px;
            }

            h3 {
                font-size: 1.2rem;
            }
        </style>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <form id="txt" onsubmit="return false;" action="{{route('count')}}" method="post" class="mx-5">
                    {{ csrf_field() }}
                    <div class="input-group m-2">
                        <textarea id="ft" rows="10" cols="45" class="form-control" name="f">{{@$f}}</textarea>
                    </div>
                    <div class="input-group m-2">
                        <textarea id="st" rows="10" cols="45" class="form-control" name="s">{{@$s}}</textarea>
                    </div>
                    <div class="input-group m-2 d-flex justify-content-center">
                        <button onclick="submitFrm()" name="compare" class="btn btn-secondary mx-2">Сравнить</button>
                        <button onclick="clearFrm()" class="btn btn-danger mx-2">Очистить</button>
                    </div>
                </form>
                <div class="title m-b-md mx-4">
                    <?php
                    echo '<h3>% совпадения текстов (похожесть второго текста на первый)</h3>';

                    echo '<pre>';
                    print_r(strval(number_format(round($p, 2), 2, '.', ''))."% \n");
                    echo '</pre>';

                    echo '<pre>';
                    print_r(strval(number_format(round($rt, 8), 8, '.', ''))." сек. \n");
                    echo '</pre>';
                    ?>
                </div>
            </div>
        </div>

        <script>
            function clearFrm() {
                    document.getElementById('ft').value = '';
                    document.getElementById('st').value = '';
            }
            function submitFrm() {
                document.getElementById('txt').submit();
            }
        </script>
    </body>
</html>
