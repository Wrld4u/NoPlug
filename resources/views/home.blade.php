@extends('layouts.app', ['title' => 'Личный кабинет'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <h2 class="text-left">Личный кабинет, добро пожаловать {{ Auth::user()->name }} !</h2>
            <h4>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
            </h4>
        </div>
    </div>

    <div class="row justify-content-center mt-5">
        <div class="col">
            <h4 class="text-center">Количество клиентов: {{$orgC ?? 0}}. Вакансий клиентов: {{$orgVacC ?? 0}}. Похожих вакансий: {{$similarC ?? 0}}</h4>
            {{--<h4 class="text-center">Количество клиентов: {{$orgC ?? 0}}. Вакансий клиентов: {{$orgVacC ?? 0}}. Всего вакансий: {{$totalVacC ?? 0}}. Похожих вакансий: {{$similarC ?? 0}}</h4>--}}
            <p>Выполнено: @{{ orgStat }} %</p>
            <p>info: id: @{{ allVacStat.id }}, name: @{{ allVacStat.name }}</p>
        </div>
    </div>
    <div class="row justify-content-center mt-5">
        <div class="col-auto">
            {{--Таблица организаций--}}
            <org></org>
        </div>
    </div>
    <div class="row justify-content-center mt-5">
        <div class="col-auto">
            {{--Таблица похожих вакансий--}}
            {{--<similar></similar>--}}
        </div>
    </div>

    <div class="row justify-content-center mt-5">
        <div class="col d-flex justify-content-center">
            {{--<a href="/api/tmp" class="btn btn-secondary"> ShowCompareTable </a>--}}

            {{--<a href="/api/areas" class="btn btn-secondary"> Регионы </a>--}}
            {{--<a href="/api/metro" class="btn btn-success"> Метро </a>--}}
            {{--<a href="/api/orgList" class="btn btn-success"> Получить организации </a>--}}
            {{--<button @click="getOrgs" class="btn btn-success">Test Получить организации @{{ orgStat }}</button>--}}
            {{--<a href="/api/allVac" class="btn btn-success"> Все вакансии </a>--}}

            {{--<button @click="getAllVac" class="btn btn-success mx-1">  Все вакансии POST </button>--}}

                {{--Test externals posts--}}
            {{--<button @click="getOrgsExt" class="btn btn-success mx-1" > Загрузить клиентов POST </button>--}}
            {{--<a href="/api/refreshVac" class="btn btn-success"> Получить OrgVac POST </a>--}}
            <button @click="getOrgsVacExt" class="btn btn-success mx-1" > Клиенты, обновить вакансии POST</button>
            {{--<a href="/api/compare" class="btn btn-info"> Сравнить </a>--}}

            {{--<a href="javascript:void(0)" @click="comp" class="btn btn-info"> Сравнить </a>--}}

            {{--<a href="/api/compare" class="btn btn-info"> Сравнить </a>--}}
            <button @click="comp" class="btn btn-success mx-1"> Сравнить POST</button>
            {{--<button @click="getSim(0)" class="btn btn-success mx-1"> GetSim </button>--}}

        </div>
    </div>

    <div class="row justify-content-center mt-5">
        <div class="col d-flex justify-content-center">

        </div>
    </div>

    <div class="row justify-content-center mt-5">
        <div class="col">
            {{--@if (isset($worker))--}}
                {{--<h2>{{($worker ?? '')}}</h2>--}}
            {{--@endif--}}
            {{--@if (isset($payload))--}}
                {{--<h2>{{($payload ?? '')}}</h2>--}}
            {{--@endif--}}
            {{--@if (isset($test))--}}
                {{--<h2>{{($test ?? '')}}</h2>--}}
            {{--@endif--}}


            @if (isset($tmp))
                {{--{{dd($tmp ?? '')}}--}}
            @endif
            {{--получаем id организаций из url их вакансий--}}
            @if (isset($orgIdList))
{{--                {{dd($orgIdList)}}--}}
                {{--<pre>--}}
                {{--{{print_r($orgIdList)}}--}}
                {{--</pre>--}}
                <h3><b>Затрачено времени: {{strval(number_format(round($idListTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>id и Название организации</b></h3>
                @foreach($orgIdList['ok'] as $key => $val)
                    <p><a href="/api/orgVac/{{$key}}">{{$key}} : {{$val}}</a></p>
                    {{--<p>{{$key}} => {{$val}}</p>--}}
                @endforeach
                <h3><b>Вакансий уже нет</b></h3>
                @foreach($orgIdList['noData'] as $val)
                    <p>{{$val}}</p>
                @endforeach
            @endif

            {{--Получаем список кратких вакансий выбранной организации--}}
            @if (isset($orgVac['items'][0]['employer']['name']))
                <h3><b>Затрачено времени: {{strval(number_format(round($vacTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>{{$orgVac['items'][0]['employer']['name']}}</b></h3>
                {{--{{dd($orgVac)}}--}}
                <pre>
                    {{print_r($orgVac['items'][0])}}
                </pre>
                <hr>
                @foreach($orgVac['items'] as $item)
                    <p><a href="/api/vac/{{$item['id']}}">id: {{$item['id']}}, Вакансия: {{$item['name']}}, Город: {{$item['area']['name'] ?? ''}}</a></p>
                @endforeach
            @elseif (isset($orgVac))
                <h3><b>Затрачено времени: {{strval(number_format(round($vacTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>За последние 7 дней организация не размещала вакансий</b></h3>
            @endif

            {{--/api/vac/{id} одна вакансия или набор--}}
            @if (isset($res))
                <h3><b>Затрачено времени: {{strval(number_format(round($oneVacTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>Вакансия</b></h3>
                {{--<pre>--}}
                {{--{{print_r($res)}}--}}
                {{--</pre>--}}
                <p>id: {{$res['id']}}</p>
                <p>Вакансия: {{$res['name']}}</p>
                <p>Описание: {{htmlspecialchars_decode(strip_tags($res['description']))}}</p>
            @endif

            {{--/api/areas справочник стран--}}
            @if (isset($resAreas))
                <h3><b>Затрачено времени: {{strval(number_format(round($areasTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>Справочник регионов</b></h3>
                <pre>
                {{print_r($resAreas)}}
                </pre>
            @endif

            {{--/api/metro справочник стран--}}
            @if (isset($resMetro))
                <h3><b>Затрачено времени: {{strval(number_format(round($metroTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>Справочник регионов</b></h3>
                <pre>
{{--                    {{dd($resMetro)}}--}}
                {{dd($resMetro)}}
                </pre>
            @endif

            {{--/api/allVac все вакансии--}}
            @if (isset($allVac))
                <h3><b>Затрачено времени: {{strval(number_format(round($allVacTime, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>Все вакансии</b></h3>
                <h3>{{count($allVac['items']) ?? ''}}</h3>
                {{dd($allVac)}}
                {{--<pre>--}}
                {{--{{print_r($allVac)}}--}}
                {{--</pre>--}}
                @foreach($allVac['items'] as $item)
                    <p><a href="/api/vac/{{$item['id']}}">id: {{$item['id']}}, Вакансия: {{$item['name']}}, Город: {{$item['area']['name'] ?? ''}}, Организация: {{$item['employer']['name']}}</a></p>
                @endforeach
            @endif

            {{--/api/compare сравнение--}}
            @if (isset($comp))
                <h3><b>Затрачено времени: {{strval(number_format(round($compT, 8), 8, '.', ''))." сек."}}</b></h3>
                <h3><b>Совпадение более 80%</b></h3>
                {{dd($comp)}}
                {{--<pre>--}}
                {{--{{print_r($allVac)}}--}}
                {{--</pre>--}}
                {{--@foreach($allVac['items'] as $item)--}}
                    {{--<p><a href="/api/vac/{{$item['id']}}">id: {{$item['id']}}, Вакансия: {{$item['name']}}, Город: {{$item['area']['name'] ?? ''}}, Организация: {{$item['employer']['name']}}</a></p>--}}
                {{--@endforeach--}}
            @endif

        </div>
    </div>
</div>
@endsection
