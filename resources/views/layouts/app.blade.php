<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @isset($title)
            {{ $title }} |
        @endisset
        {{ config('app.name') }}
    </title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--Favicon-->
    {{--<link rel="shortcut icon" href="favicon.png" />--}}
</head>
<body>
{{--<button onclick="topFunction()" id="toTop" title="Go to top"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></button>--}}

<div id="app">

    <header>
        @include('partials.header')
    </header>

    <main class="py-4">
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-9">--}}
                    @yield('content')
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </main>
</div> <!-- id="app" -->

<footer>
    {{--@include('partials.footer')--}}
</footer>

<script>
    // //ToTop ==== start
    // window.onscroll = function() {scrollFunction()};
    //
    // function scrollFunction() {
    //     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    //         document.getElementById("toTop").style.display = "block";
    //     } else {
    //         document.getElementById("toTop").style.display = "none";
    //     }
    // }
    //
    // // When the user clicks on the button, scroll to the top of the document
    // function topFunction() {
    //     $('html, body').animate({scrollTop: 0}, 1000);
    //     // document.body.scrollTop = 0;
    //     // document.documentElement.scrollTop = 0;
    // }
    // //ToTop ==== end
</script>

</body>
</html>
