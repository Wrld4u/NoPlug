/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('org', require('./components/Org.vue').default);
Vue.component('similar', require('./components/Similar.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        href:'',
        orgStat: '',
        allVacStat: '',
    },
    methods:{
        comp: function () {
            axios.post('/api/compareExt',{})
                .then(function (response) {
                    console.log('response: '+JSON.stringify(response.data));
                });
        },
        getOrgs: function () {
            axios.post('/api/orgList');
        },
        getAllVac: function () {
            axios.post('/api/allVac');
        },
        getOrgsExt: function () {
            let urls = [
                "https://hh.ru/employer/1947330",
                "https://hh.ru/employer/1111058",
                "https://hh.ru/employer/2735486",
                "https://hh.ru/employer/1947314",
                "https://hh.ru/employer/659638",
                "https://hh.ru/employer/913195",
                "https://hh.ru/employer/247118",
                "https://hh.ru/employer/25022",
                "https://hh.ru/employer/2765",
                "https://hh.ru/employer/2009",
                "https://hh.ru/employer/37158",
                "https://hh.ru/employer/124",
                "https://hh.ru/employer/654435",
                "https://hh.ru/employer/1483209",
                "https://hh.ru/employer/864086",
                "https://hh.ru/employer/333",
                "https://hh.ru/employer/1846",
                "https://hh.ru/employer/1068",
                "https://hh.ru/employer/89537",
                "https://hh.ru/employer/2495431",
                "https://hh.ru/employer/2538",
                "https://hh.ru/employer/67952",
                "https://hh.ru/employer/180",
                "https://hh.ru/employer/1519245",
                "https://hh.ru/employer/3524588",
            ];
            axios.post('/api/addusers', {urls: urls})
                .then((response) => {
                    console.log(response.data);
                });
        },
        getOrgsVacExt: function () {
            axios.post('/api/refreshVac')
                .then((response) => {
                        console.log(response.data);
                    });
        },
        getSim: function (id) {
            axios.post('/api/getSim', {id: id})
                .then((response) => {
                    console.log(response.data);
                });
        },

    },
    mounted: function () {
        window.Echo.channel('org-status')
            .listen('GetOrgStat', (status) => {
                this.orgStat = status.status;
                // console.log(status);
            });
        window.Echo.channel('vac-status')
            .listen('GetVacStat', (status) => {
                this.allVacStat = status.status;
                if (typeof this.allVacStat.name !== 'undefined' && typeof this.allVacStat.more !== 'undefined' && this.allVacStat.more > 0) {
                    console.log(this.allVacStat.name, this.allVacStat.more)
                }
                // console.log(status);
            });

    },
    created: function () {
        this.href = window.location.protocol+'//'+ window.location.host+'/';
    },
});
