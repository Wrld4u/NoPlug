<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function (){
  return view('start', ['title'=> 'Войдите или зарегистрируйтесь']);
});

//Route::post('/2', function (Request $request) {
//    $start = microtime(true);
//    if (!isset($request->f) || !isset($request->s)) {
//        $f = normalize_np("Крупная международная компания, производитель одних из самых лучших автомобилей класса люкс в мире, готова рассмотреть кандидатов на позицию Project manager для организации работ по реконструкции здания головного офиса в Москве.");
////         $s = "Крупная компания, производитель самых лучших автомобилей класса люкс в мире, готова рассмотреть кандидатов на позицию Project manager для организации работ.";
//        $s = normalize_np("ГК ПОРШЕ требуется осуществить комплексный проект реконструкции здания (включая полную замену коммуникаций и систем). Работа компании при этом не останавливается.По данному проекту необходим будет полный цикл работ, начиная от проектной документации, разработки рабочей документации, получения всех разрешений и курирование всех этапов работ до момента окончания реконструкции.");
//    } else {
//        $f = normalize_np($request->f);
//        $s = normalize_np($request->s);
//    }
//
//    $similarity1 = similar_text($f, $s, $p);
////    $p = 0;
//    $resTime = (microtime(true) - $start);
//
//    return view('tmp.welcome', ['f'=>$f, 's'=>$s, 'p'=>$p, 'rt'=>$resTime]);
//})->name('count');
//
//Route::get('/1', function (Request $request) {
//
//    if (!isset($request->f) || !isset($request->s)) {
//        $f = ("Крупная международная компания, производитель одних из самых лучших автомобилей класса люкс в мире, готова рассмотреть кандидатов на позицию Project manager для организации работ по реконструкции здания головного офиса в Москве.");
////         $s = "Крупная компания, производитель самых лучших автомобилей класса люкс в мире, готова рассмотреть кандидатов на позицию Project manager для организации работ.";
//        $s = ("ГК ПОРШЕ требуется осуществить комплексный проект реконструкции здания (включая полную замену коммуникаций и систем). Работа компании при этом не останавливается.По данному проекту необходим будет полный цикл работ, начиная от проектной документации, разработки рабочей документации, получения всех разрешений и курирование всех этапов работ до момента окончания реконструкции.");
//    } else {
//        $f = $request->f;
//        $s = $request->s;
//    }
//
//
//    return view('tmp.welcome', ['f'=>$f, 's'=>$s, 'p'=>0, 'rt'=>0]);
//})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'namespace'     => 'Ext',
    'middleware'    =>  ['auth']], function () {
    Route::get('/api/vac/{id}', 'Hh@showVac');
//    Route::get('/api/test', 'HhApi@updOrgList'); //used for update org when add site field
    Route::post('/api/orgList', 'Hh@getOrgIdList');
//    Route::get('/api/orgList', 'Hh@getOrgIdList');
    Route::get('/api/orgVac/{id}', 'Hh@getOrgVac');
    Route::get('/api/areas', 'Hh@showAreas');
    Route::get('/api/metro', 'Hh@showMetro');

    Route::get('/api/compare', 'Hh@compare');

    Route::post('/api/orgTable', 'Hh@showOrgTable');
    Route::post('/api/compareRes', 'Hh@showCompareTable');
    //External
//    Route::post('/api/addusers', 'Hh@setNewUsers');
//    Route::post('/api/refreshVac', 'Hh@refreshVac'); //cron
//    Route::post('/api/allVac', 'Hh@allVac'); //cron
//    Route::post('/api/compareExt', 'Hh@compareExt'); //cron

    //* - get обнуляет счётчик сёрфинга для квалификации (curl mystart.today/ajax/updateSkill/zsxd124vjhv/rmSerf)
    //* - get уменьшает на 1 квалификацию у всех пользователей (меньше 1 не становится) (curl mystart.today/ajax/updateSkill/zsxd124vjhv/rmSkill)
    //curl -X POST http://matcher.recruitmnent.ru//api/allVac для крона post

});

Route::group([
    'namespace'     => 'Ext',
    'middleware'    =>  ['api']], function () {
    //External
    Route::post('/api/addusers', 'Hh@setNewUsers'); //add User
    Route::post('/api/rmusers', 'Hh@rmUsers'); //delete User
    Route::post('/api/getSim', 'Hh@getSim'); // get Results
    Route::post('/api/allVac', 'Hh@allVac'); //cron not Used because we don't collect all vacancies and compare it. We just use hh.api to search similar vacancies
    Route::post('/api/getContacts', 'Hh@getContacts'); // return contacts linked to emp_id and site from resume.hr_hr
    Route::get('/api/getContForCSV', 'Hh@getContForCSV'); // return csv with contacts filtered by request params

    Route::post('/api/refreshVac', 'Hh@refreshVac'); //cron refresh clients vacancies
    Route::post('/api/compareExt', 'Hh@compareExt'); //cron search vacancies similar to clients
});
