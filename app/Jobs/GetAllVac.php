<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Ext\HhApi;

class GetAllVac implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mt;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mt)
    {
        $this->mt = $mt;
    }

    /**
     * Execute the job.
     *
     * @param HhApi $hh
     * @return void
     */
    public function handle(HhApi $hh)
    {
        $hh->getAllVacancies($this->mt);
//        Compare::dispatch()->onQueue('workers');
    }
}
