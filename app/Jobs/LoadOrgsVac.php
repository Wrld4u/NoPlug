<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Ext\HhApi;


class LoadOrgsVac implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $urls;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($urls)
    {
        $this->urls = $urls;
    }

    /**
     * Execute the job.
     *
     * @param HhApi $hh
     * @return void
     */
    public function handle(HhApi $hh)
    {
        $res = $hh->getOrgList($this->urls);
        $hh->fillAllShortVacancies($res['ok']); //[ id => name ]
    }
}
