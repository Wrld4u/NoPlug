<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Ext\Models\OrgVacancy as OrgVac;
use App\Http\Controllers\Ext\Models\TotalVacancy as TotalVac;
use App\Http\Controllers\Ext\Models\Similar;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Events\GetOrgStat;
use App\Events\GetVacStat;
use App\Jobs\CompareWorker;

class Compare implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        protected $fillable = ['parent_vacancy_id', 'vacancy_id', 'name', 'url', 'text', 'mod_text', 'checked'];
        /**
         * Сравниваем
         *
         *   vvvvvvvv - Org Vacancy id
         *   34275626 => array:4 [▼
         *       0 => array:6 [▼
         *            "vacancy_id" => 27186324
         *            "name" => "Продавец-консультант Juicy Couture"
         *            "url" => "https://hh.ru/vacancy/27186324"
         *            "mod_text" => "жлтлнспшнпрджхктвнжзнннпзцмнрбттькмнджлндстгтьрзльттстнвлвтьсдстгнтмпрдствлнклнтскгсрвсклссстндртмкмпнрзвтдлгсрчнхтншнклнтмсщствлнвклдкпрдкцкмпнмгзн"
         *            "text" => "Желателен успешный опыт в продажах. Активная жизненная позиция. Умение работать в команде. Желание достигать результата и не останавливаться на достигнутом. Предоставление клиентского сервиса класса люкс по стандартам компании. Развитие долгосрочных отношений с клиентами. Осуществление выкладки продукции компании в магазине.  ◀"
         *            "main text" => "Желателен успешный опыт в продажах. Активная жизненная позиция. Умение работать в команде. Желание достигать результата и не останавливаться на достигнутом. Предоставление клиентского сервиса класса люкс по стандартам компании. Развитие долгосрочных отношений с клиентами. Осуществление выкладки продукции компании в магазине.  ◀"
         *            ]
         *       1 => array:6 [▶]
         *       2 => array:6 [▶]
         *       3 => array:6 [▶]
         *       ]
         *   31864045 => array:4 [▶]
         */


//        $main = OrgVac::all(['vacancy_id', 'name', 'url', 'mod_text', 'parent_id', 'text'])->toArray();
        $main = OrgVac::whereDate('updated_at', '>=', Carbon::now()->subWeeks(1))->get(['vacancy_id', 'name', 'url', 'mod_text', 'parent_id', 'text'])->toArray();

//        $res = [];
        $total = count($main);
        $c = 0;
        foreach ($main as $mVac){
            GetOrgStat::dispatch(round($c*100/$total, 2));
            $c++;

            CompareWorker::dispatch($mVac)->onQueue('compare');

//            TotalVac::chunk(300, function ($sec) use ($mVac) {
////            TotalVac::whereDate('created_at', '>=', Carbon::now()->subWeeks(2))->chunk(300, function ($sec) use ($mVac) {
//
//                foreach ($sec as $sVac) {
//                    $sVac = $sVac->toArray();
//                    $exist = Similar::where('parent_vacancy_id', $mVac['vacancy_id'])->where('vacancy_id', $sVac['vacancy_id'])->count();
//                    if ($mVac['vacancy_id'] != $sVac['vacancy_id'] &&  $exist == 0 && $sVac['checked'] == 0) { //если это не таже вакансия
//                        similar_text($mVac['mod_text'], $sVac['mod_text'], $p);
//                        if ($p > 70) {
//                            //Сохранение в БД
////                            $sim = new Similar;
////                            $sim->parent_vacancy_id = $mVac['vacancy_id'];
////                            $sim->vacancy_id = $sVac['vacancy_id'];
//                            $sim = Similar::firstOrNew(['parent_vacancy_id' => $mVac['vacancy_id'], 'vacancy_id' => $sVac['vacancy_id']]);
//                            $sim->name = $sVac['name'];
//                            $sim->url = $sVac['url'];
//                            $sim->text = $sVac['text'];
//                            $sim->mod_text = $sVac['mod_text'];
//
//                            $sim->save();
//
//                        }
//                    }
//                }
//            });

        }

    }
}
