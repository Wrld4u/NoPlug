<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\Ext\HhApi;

use App\Events\GetOrgStat;
use App\Events\GetVacStat;

class RefreshOrgVac implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orgs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orgs)
    {
        $this->orgs = $orgs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HhApi $hh)
    {

//        GetOrgStat::dispatch('start');
        $hh->fillAllShortVacancies($this->orgs); //[ id => name ]
        GetOrgStat::dispatch('finish');

    }
}
