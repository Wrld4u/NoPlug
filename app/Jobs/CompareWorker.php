<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Http\Controllers\Ext\Models\TotalVacancy as TotalVac;
use App\Http\Controllers\Ext\Models\OrgVacancy as OrgVac;
use App\Http\Controllers\Ext\Models\Similar;
use Illuminate\Support\Facades\DB;
use App\Events\GetOrgStat;
use Carbon\Carbon;


class CompareWorker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mVac;

    /**
     * Create a new job instance.
     *
     * @param $mVac
     */
    public function __construct($mVac)
    {
        $this->mVac = $mVac;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //добавить отображение % (кол-во оставшихся в таблице Job работ * 100 / кол-во вакансий клиентов)
        $ov = OrgVac::all()->count();
        $ost = DB::table(config('queue.connections.database.table'))->where('queue', 'compare')->count();
        GetOrgStat::dispatch(round(100-($ost*100/$ov), 2));

        $mVac = $this->mVac;
//        TotalVac::chunk(300, function ($sec) use ($mVac) {
        TotalVac::whereDate('updated_at', '>=', Carbon::now()->subWeeks(4))->chunk(300, function ($sec) use ($mVac) {

            foreach ($sec as $sVac) {
                $sVac = $sVac->toArray();
                $exist = Similar::where('parent_vacancy_id', $mVac['vacancy_id'])->where('vacancy_id', $sVac['vacancy_id'])->count();
                if ($mVac['vacancy_id'] != $sVac['vacancy_id'] &&  $exist == 0 && $sVac['checked'] == 0) { //если это не таже вакансия
                    similar_text($mVac['mod_text'], $sVac['mod_text'], $p);
                    if ($p > 80) {
                        //Сохранение в БД
//                            $sim = new Similar;
//                            $sim->parent_vacancy_id = $mVac['vacancy_id'];
//                            $sim->vacancy_id = $sVac['vacancy_id'];
                        $sim = Similar::firstOrNew(['parent_vacancy_id' => $mVac['vacancy_id'], 'vacancy_id' => $sVac['vacancy_id']]);
                        $sim->name = $sVac['name'];
                        $sim->url = $sVac['url'];
                        $sim->text = $sVac['text'];
                        $sim->mod_text = $sVac['mod_text'];

                        $sim->save();

                    }
                }
            }
        });
    }
}
