<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Http\Controllers\Ext\Models\OrgVacancy as OrgVac;
use App\Http\Controllers\Ext\Models\Similar;
use Illuminate\Support\Facades\DB;
use App\Events\GetOrgStat;
use Carbon\Carbon;


class FilterSimilar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mVac;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mVac)
    {
        $this->mVac = $mVac;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //добавить отображение % (кол-во оставшихся в таблице Job работ * 100 / кол-во вакансий клиентов)
        $ov = OrgVac::all()->count();
        $ost = DB::table(config('queue.connections.database.table'))->where('queue', 'compare')->count();
        GetOrgStat::dispatch(round(100-($ost*100/$ov), 2));

        $mVac = $this->mVac;

        $sims = Similar::where('parent_vacancy_id', '=', $mVac['vacancy_id'])->get();
        foreach ($sims as $sim) {
            similar_text($mVac['mod_text'], $sim['mod_text'], $p);
            if ($p < 50) {
                $sim->delete();
            }

        }

    }
}
