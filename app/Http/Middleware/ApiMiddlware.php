<?php

namespace App\Http\Middleware;

use Closure;

class ApiMiddlware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next( $request );

        $response->headers->set( 'Access-Control-Allow-Origin', '*' );
        $response->headers->set( 'Access-Control-Allow-Headers', 'Origin, Content-Type, Content-Disposition, Pragma, Expires' );
//        $response->header( 'Content-Type', 'application/json' );

        return $response;
    }
}
