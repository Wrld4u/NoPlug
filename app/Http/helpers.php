<?php
/**
 * Created by PhpStorm.
 * User: AdminCM
 * Date: 24.11.2019
 * Time: 15:18
 */

if (! function_exists('normalize_np')) {
    function normalize_np($txt)
    {

//        $res = $txt;
        $res = mb_convert_case($txt, MB_CASE_LOWER, "UTF-8");
        $res = preg_replace('/\pP|\r\n+/iu', ' ', $res); //убираем знаки препинания и переносы строк
        $res = preg_replace('/(\b(\S{1,5})\b)|([а,е,ё,и,о,у,й,э,ы,ю,я]+)/iu', '', $res); //убираем слова длинной мене 5 символов //|([а,е,ё,и,о,у,й,э,ы,ю,я]+)
        $res = preg_replace('/\s{1,}/', '', $res); //заменяем более 1го пробела подряд на 1

//        $resM = '';
//        $wrd = explode(' ', $res);
//        foreach ($wrd as $word){
//            $resM .= metaphone_rus($word).' ';
//        }

        return $res;
    }
}
