<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Ext\Models\Org as Org;
use App\Http\Controllers\Ext\Models\OrgVacancy as OrgVac;
use App\Http\Controllers\Ext\Models\TotalVacancy as TotalVac;
use App\Http\Controllers\Ext\Models\Metro as Metro;
use App\Http\Controllers\Ext\Models\Similar;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $org = Org::where(['org_id' => 3726])->with('vacancy')->get(); //выберет 3726 организацию вместе с вакансиями
//        $org = Org::where(['org_id' => 3726])->first()->vacancy; //если таблица пуста -> глючит иначе даст только вакансии

        //Проверка на пустоту предыдущего варианта
//        $org = Org::where(['org_id' => 3726])->first();
//        if (isset($org)) $res = $org->vacancy; else $res = 'Нет вакансий';

//        $res = Metro::all()->toArray(); //Вот это отдавать getAllVacancies($res)

        $org = number_format(Org::count(), 0, ',', ' ');
        $orgVac = number_format(OrgVac::count(), 0, ',', ' ');
        $totalVac = number_format(TotalVac::count(), 0, ',', ' ');
        $similar = number_format(Similar::count(), 0, ',', ' ');

//        $res = DB::table(config('queue.connections.database.table'))->where('queue', '=','compare')->count();
//        $queue = DB::table(config('queue.connections.database.table'))->where('queue', '=', 'compare')->first();
//        if($queue) {
//            $payload = json_decode($queue->payload, true);
//        } else {
//            $payload = '';
//        }
//        $testStr = 'App\\Jobs\\Compare';

        return view('home', ['orgC' => $org, 'orgVacC' => $orgVac, 'totalVacC' => $totalVac, 'similarC' => $similar]);//, 'worker' => $res, 'payload' => $payload['displayName'], 'test' => $testStr]);
    }
}
