<?php

namespace App\Http\Controllers\Ext;

use App\Http\Controllers\Ext\HhApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;
use App\Http\Controllers\Ext\Models\Metro as Metro;
use App\Http\Controllers\Ext\Models\Org as Org;
use App\Http\Controllers\Ext\Models\OrgVacancy as OrgVac;
use App\Http\Controllers\Ext\Models\TotalVacancy as TotalVac;
use App\Http\Controllers\Ext\Models\Similar;
use App\Jobs\LoadOrgsVac;
use App\Jobs\GetAllVac;
use App\Jobs\RefreshOrgVac;
use App\Jobs\Compare;
use App\Jobs\CompareAPI;
use App\Events\GetOrgStat;
use App\Events\GetVacStat;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;



class Hh extends Controller
{

    /**
     * Получаем через api список ссылок на организации
     * и извлекаем из актуальных ссылок id организации
     * неактуальные отмечаем
     * @param array $urls
     * @param \App\Http\Controllers\Ext\HhApi $hh
     * @return void
     */
    function getOrgIdList($urls = [], HhApi $hh) {
//        $start = microtime(true);

//        if (empty($urls)) $urls = array(
//            'https://hh.ru/employer/1947330',
//            'https://hh.ru/employer/1111058',
//            'https://hh.ru/employer/2735486',
//            'https://hh.ru/employer/1947314',
//            'https://hh.ru/employer/659638',
//            'https://hh.ru/employer/913195',
//            'https://hh.ru/employer/247118',
//            'https://hh.ru/employer/25022',
//            'https://hh.ru/employer/2765',
//            'https://hh.ru/employer/2009',
//            'https://hh.ru/employer/37158',
//            'https://hh.ru/employer/124',
//            'https://hh.ru/employer/654435',
//            'https://hh.ru/employer/1483209',
//            'https://hh.ru/employer/864086',
//            'https://hh.ru/employer/333',
//            'https://hh.ru/employer/1846',
//            'https://hh.ru/employer/1068',
//            'https://hh.ru/employer/89537',
//            'https://hh.ru/employer/2495431',
//            'https://hh.ru/employer/2538',
//            'https://hh.ru/employer/67952',
//            'https://hh.ru/employer/180',
//            'https://hh.ru/employer/1519245',
//            'https://hh.ru/employer/3524588',
//        );

//        $res = $hh->getOrgList($urls);
//        $hh->fillAllShortVacancies($res['ok']); //[ id => name ]

//        GetOrgStat::dispatch('start');
        LoadOrgsVac::dispatch($urls)->onQueue('workers');
//        GetOrgStat::dispatch('finish');

//        $resTime = (microtime(true) - $start);
//        return view('home', ['orgIdList' => $res]);//, 'idListTime' => $resTime]);
    }

    /**
     * Получаем список вакансий организации (краткое описание вакансии)
     * @param $id
     * @param \App\Http\Controllers\Ext\HhApi $hh
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function getOrgVac($id, HhApi $hh) {
        $start = microtime(true);

        $res = $hh->getShortVacancies($id);
        $resTime = (microtime(true) - $start);
        return view('home', ['orgVac' => $res, 'vacTime' => $resTime]);
    }


    /**
     * Получение конкретной вакансии
     * @param $id
     * @param \App\Http\Controllers\Ext\HhApi $hh
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function showVac($id, HhApi $hh) {
        $start = microtime(true);

        $res = $hh->getFullVacancy($id);
        $resTime = (microtime(true) - $start);
        return view('home', ['res' => $res, 'oneVacTime' => $resTime]);
    }

    /**
     * Справочник стран и городов
     * @param \App\Http\Controllers\Ext\HhApi $hh
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function showAreas(HhApi $hh) {
        $start = microtime(true);

        $res = $hh->getAreas(113); //Россия
        $resTime = (microtime(true) - $start);
        return view('home', ['resAreas' => $res, 'areasTime' => $resTime]);
    }

    /**
     * Справочник метро
     * @param \App\Http\Controllers\Ext\HhApi $hh
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function showMetro(HhApi $hh) {
        $start = microtime(true);

        $res = $hh->getMetro(1); //City id, 1 MSK, 2 SPb
        $hh->fillMetro(1);
        $hh->fillMetro(2);
        $resTime = (microtime(true) - $start);
        return view('home', ['resMetro' => $res, 'metroTime' => $resTime]);
    }

    /**
     * Сравниваем
     *
     *   vvvvvvvv - Org Vacancy id
     *   34275626 => array:4 [▼
     *       0 => array:6 [▼
     *            "vacancy_id" => 27186324
     *            "name" => "Продавец-консультант Juicy Couture"
     *            "url" => "https://hh.ru/vacancy/27186324"
     *            "mod_text" => "жлтлнспшнпрджхктвнжзнннпзцмнрбттькмнджлндстгтьрзльттстнвлвтьсдстгнтмпрдствлнклнтскгсрвсклссстндртмкмпнрзвтдлгсрчнхтншнклнтмсщствлнвклдкпрдкцкмпнмгзн"
     *            "text" => "Желателен успешный опыт в продажах. Активная жизненная позиция. Умение работать в команде. Желание достигать результата и не останавливаться на достигнутом. Предоставление клиентского сервиса класса люкс по стандартам компании. Развитие долгосрочных отношений с клиентами. Осуществление выкладки продукции компании в магазине.  ◀"
     *            "main text" => "Желателен успешный опыт в продажах. Активная жизненная позиция. Умение работать в команде. Желание достигать результата и не останавливаться на достигнутом. Предоставление клиентского сервиса класса люкс по стандартам компании. Развитие долгосрочных отношений с клиентами. Осуществление выкладки продукции компании в магазине.  ◀"
     *            ]
     *       1 => array:6 [▶]
     *       2 => array:6 [▶]
     *       3 => array:6 [▶]
     *       ]
     *   31864045 => array:4 [▶]
     *
     *
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function compare() {
        ini_set("memory_limit", "512M");
        $start = microtime(true);

        $main = OrgVac::all(['vacancy_id', 'name', 'url', 'mod_text', 'parent_id', 'text'])->toArray();
//        $sec = TotalVac::all(['vacancy_id', 'name', 'url', 'mod_text', 'text'])->toArray();
//        $main = OrgVac::take(10)->get(['vacancy_id', 'name', 'url', 'mod_text', 'parent_id', 'text'])->toArray();
        $sec = TotalVac::skip(5000)->take(5000)->get(['vacancy_id', 'name', 'url', 'mod_text', 'text'])->toArray();

        $res = [];
        foreach ($main as $mVac){

            //Этот цикл должен быть разбит чанками
            foreach ($sec as $sVac){
                if ($mVac['vacancy_id'] != $sVac['vacancy_id']) { //если это не таже вакансия
                    similar_text($mVac['mod_text'], $sVac['mod_text'], $p);
                    if ($p > 80) {
                        $sVac['main text'] = $mVac['text'];
                        $res[$mVac['vacancy_id']][] = $sVac;
                    }
                }
            }

        }

        $resTime = (microtime(true) - $start);
//        return response()->json(['comp' => $res, 'compT' => $resTime]);
        return view('home', ['comp' => $res, 'compT' => $resTime]);
    }





//    External posts start =============================================================================

    /**
     * По внешнему запросу добавляем Организации и их вакансии (+Save)
     * Сразу запустит сравнение
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function setNewUsers(Request $request) {
        if (isset($request->urls)) {
            LoadOrgsVac::dispatch($request->urls)->onQueue('workers');
//            if (!(new HhApi)->checkJob('App\\Jobs\\Compare') && !(new HhApi)->checkJobInQueue('compare')) {
//                Compare::dispatch()->onQueue('workers');
//            }
            return response()->json(['200' => 'ok']);
        } else {
            return response()->json(['err' => 'Ошибка, список пуст', 'urls' => $request->urls]);
        }
    }

    /**
     * По внешнему запросу убираем Организации
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function rmUsers(Request $request) {
        if (isset($request->org_id)) {
            // remove companies from db
            $orgs = Org::where('org_id', $request->org_id)->get();
            if (isset($orgs)) {
                foreach ($orgs as $org) {
                    $org->vacancy()->delete();
                    $org->delete();
                }
            }
            return response()->json(['200' => 'ok']);
        } else {
            return response()->json(['err' => 'Ошибка', 'urls' => $request->org_id]);
        }
    }

    public function toString($sim)
    {
        $str = [];
        $i = 0;
        if (!empty($sim)) {
            foreach ($sim as $company) {
                if (!empty($company)) {
                    if (!empty($company['similars'])) {
                        foreach ($company['similars'] as $compSim) {
                            foreach ($compSim as $vac) {
                                $str[$i]['client_name'] = $company['client_name'];
                                $str[$i]['client_id'] = $company['client_id'];
                                $str[$i]['contact_id'] = $company['contact_id'];

                                $str[$i]['id'] = $vac['id'];
                                $str[$i]['parent_vacancy_id'] = $vac['parent_vacancy_id'];
                                $str[$i]['vacancy_id'] = $vac['vacancy_id'];
                                $str[$i]['emp_id'] = $vac['emp_id'];
                                $str[$i]['emp_site'] = $vac['emp_site'];
                                $str[$i]['name'] = $vac['name'];
                                $str[$i]['url'] = $vac['url'];
                                $str[$i]['text'] = $vac['text'];
                                $str[$i]['created_at'] = $vac['created_at'];
                                $str[$i]['updated_at'] = $vac['updated_at'];
                                $i++;
                            }
                        }
                    }
                }
            }
        }

        return $str;
    }


     /**
     * По внешнему запросу отдаём похожие вакансии или вакансию (если id != 0)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getSim(Request $request) {

        if ($request->id != 0){
            $org = Org::where(['org_id' => $request->id])->first();
            if (isset($org)) {
                $vacs = $org->vacancy;
                $res = [];

                // Сохраняем компанию
                $res[0]['client_name'] = $org->name;
                $res[0]['client_id'] = $org->org_id;
                $res[0]['contact_id'] = $org->contact_id;
                $res[0]['similars'] = [];
                foreach ($vacs as $vac){
                    $tmp = Similar::where(['parent_vacancy_id' => $vac->vacancy_id])->whereDate('updated_at', '>=', Carbon::now()->subWeeks(1))->get()->toArray();
                    if (!empty($tmp)) {
                        array_push($res[0]['similars'], $tmp);
                    }
                }
                return response()->json($this->toString($res));//, 200, [], JSON_FORCE_OBJECT);
//                return response()->json($res);
            }
        } else {
            //Вложенная жадная загрузка
            $orgs = Org::with('vacancy.sim')->get();

            if (isset($orgs)) {
                $res = [];
                $i = 0;
                foreach ($orgs as $org){
                    // Сохраняем компанию
                    $res[$i]['client_name'] = $org->name;
                    $res[$i]['client_id'] = $org->org_id;
                    $res[$i]['contact_id'] = $org->contact_id;
                    $res[$i]['similars'] = [];
                    foreach ($org->vacancy as $vac){
                        $tmp = $vac->sim->where('updated_at', '>=', Carbon::now()->subWeeks(1))->toArray();
                        if (!empty($tmp)) {
                            array_push($res[$i]['similars'], array_slice($tmp, 0, 3));
                        }
                    }
                    if (count($res[$i]['similars']) == 0) {
                        unset($res[$i]);
                    }
                    $i++;
                }

                return response()->json($this->toString($res));//, 200, [], JSON_FORCE_OBJECT);
//                return response()->json($res);
            }
        }

//        if (empty($res)) $res = 'Нет вакансий';

        return response()->json([]);

    }


    /**
     * Получает из БД список организаций и обновляет их вакансии (+Save)
     * Сразу запустит сравнение
     * @return \Illuminate\Http\JsonResponse
     */
    function refreshVac() {
        OrgVac::query()->truncate();

        $res = Org::all(['org_id', 'name'])->pluck('name', 'org_id');
        RefreshOrgVac::dispatch($res)->onQueue('workers');

//        if (!(new HhApi)->checkJob('App\\Jobs\\Compare') && !(new HhApi)->checkJobInQueue('compare')) {
//            Compare::dispatch()->onQueue('workers');
//        }
        return response()->json(['200' => 'ok']);
    }

    /**
     * Получаем список всех доступных вакансий (+Save)
     * Сразу запустит сравнение
     * @param \App\Http\Controllers\Ext\HhApi $hh
     * @return \Illuminate\Http\JsonResponse
     */
    function allVac(HhApi $hh) {
        $mt = Metro::all()->toArray(); //Вот это отдавать getAllVacancies($res)
        //Если таблица метро пустая, то получить её от hh
        //и снова запросить из БД
        if (!isset($mt) || empty($mt)) {
            $hh->fillMetro(1);
            $hh->fillMetro(2);
            $mt = Metro::all()->toArray(); //Вот это отдавать getAllVacancies($res)
        }
//        $res = $hh->getAllVacancies($mt);
//        GetVacStat::dispatch('start');
        GetAllVac::dispatch($mt)->onQueue('workers');
        GetOrgStat::dispatch('finish');
        if (!(new HhApi)->checkJob('App\\Jobs\\Compare') && !(new HhApi)->checkJobInQueue('compare')) {
            Compare::dispatch()->onQueue('workers');
        }


        return response()->json(['200' => 'ok']);
//        return view('home', ['allVac' => $res, 'allVacTime' => $resTime]);
    }

    /**
     * Запускает сравнение (+Save)
     */
    function compareExt(HhApi $hh) {
        //prepare TotalVac
//        $orgIds = Org::all(['org_id'])->toArray();
//        foreach ($orgIds as $orgId){
//            TotalVac::chunk(300, function ($items) use ($orgId) {
//                foreach ($items as $item) {
//                    if ($item->parent_id == $orgId) {
//                        $item->checked = 1;
//                        $item->save();
//                    }
//                }
//            });
//        }
//        GetVacStat::dispatch('finished modify of TotacVac');

//        Compare::dispatch()->onQueue('workers');
        Similar::query()->truncate();

        CompareAPI::dispatch()->onQueue('workers');
//        $res = $hh->searchAllByAPI();
//        return response($res);
    }
//    External posts end   =============================================================================


    /**
     * Данные таблицы организаций
     * @return array
     */
    function showOrgTable() {
        return Org::all()->toArray();
    }

    /**
     * Данные таблицы вакансий организаций
     * @return array
     */
    function showOrgVacTable() {
        return OrgVac::all()->toArray();
    }

    /**
     * Данные таблицы схожих вакансий
     * На выходе массив из вакансий клиентов и в каждую вложен массив похожих
     *
     *   array:50 [▼
     *     34634992 => array:1 [▼
     *         0 => array:10 [▼
     *              "vacancy_id" => 33984622
     *              "name" => "Мерчендайзер (м. Таганская-м. Волгоградский проспект)"
     *              "url" => "https://hh.ru/vacancy/33984622"
     *              "mod_text" => "нлчдствщмдцнсккнжкжлнрбттьпрджрбтфдрльнмстммгнлптрчкпркрстквськвктрвклдкпрдкцmaccoffeeкссх"
     *              "text" => "Наличие действующей медицинской книжки. Желание работать в сфере продаж. Работа с федеральными сетями (Магнолия, Пятерочка, Перекресток, Дикси, Авоська, Фикс Пр ▶"
     *              "parent_vacancy_id" => 34634992
     *              "main_text" => "Наличие действующей медицинской книжки. Желание работать в сфере продаж. Работа с федеральными сетями (Магнолия, Пятерочка, Перекресток, Дикси, Авоська, Фикс Пр ▶"
     *              "main_name" => "Мерчендайзер по району Семеновская"
     *              "main_url" => "https://hh.ru/vacancy/34634992"
     *              "main_org" => "MacCoffee"
     *              ]
     *         ]
     *     34578836 => array:1 [▶]
     *   ]
     *
     */
    function showCompareTable() {
//        protected $fillable = ['parent_vacancy_id', 'vacancy_id', 'name', 'url', 'text', 'mod_text', 'checked'];

        $sim = Similar::all()->toArray();
        $res =[];
        foreach ($sim as $vac) {
            $res[$vac['parent_vacancy_id']][] = [
                'vacancy_id' => $vac['vacancy_id'],
                'name' => $vac['name'],
                'url' => $vac['url'],
                'mod_text' => $vac['mod_text'],
                'text' => $vac['text'],
                'parent_vacancy_id' => $vac['parent_vacancy_id'],
                'main_text' => OrgVac::where('vacancy_id', $vac['parent_vacancy_id'])->first()->text,
                'main_name' => OrgVac::where('vacancy_id', $vac['parent_vacancy_id'])->first()->name,
                'main_url' => OrgVac::where('vacancy_id', $vac['parent_vacancy_id'])->first()->url,
                'main_org' => OrgVac::where('vacancy_id', $vac['parent_vacancy_id'])->first()->org->name,
                ];
        }

//        return view('home', ['tmp' => $res]);
        return $res;
    }

    /**
     * Получаем emp_id или site или company или всё вместе и ищем по ним
     * от сайта оставляем только домен (без http и www) и проверяем входит ли он в домен email из таблицы resume.hh_hh
     * и входит ли домен email в сайт (проверка SQL запросом НЕ перебором)
     * Так же проверяем совпадение emp_id в двух таблицах
     *
     * http://matcher.recruitmnent.ru/api/getContacts?id=[id]&site=[site]
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getContacts(Request $request)
    {
        $cnt_id = [];
        $cnt_mail = [];
        $cnt_company = [];
        if (isset($request->id) or isset($request->site) or isset($request->company)) {
//            $sim = Similar::find($request->id);
//            $emp_id = $sim['emp_id'];
//            $site = $sim['emp_site'] ? parse_url($sim['emp_site'])['host'] : Null;
            $emp_id = isset($request->id) ? $request->id : Null;
            $site = isset($request->site) ? parse_url($request->site)['host'] : Null;
            $company = isset($request->company) ? $request->company : Null;

            if (isset($emp_id)) {
                $cnt_id = DB::connection('mysql2')->table('hr_hr')->where('emp_id', 'like', $emp_id)->get()->toArray();
            }

            if (isset($site)){
                $site = strtolower($site);
                $site = str_replace("www.ru.", "", $site);
                $site = str_replace("www.", "", $site);
                $site = str_replace(' ', '', $site);

                $cnt_mail = DB::connection('mysql2')->table('hr_hr')->where('email', 'like', '%'.$site.'%')->get()->toArray();
            }

            if (isset($company)){
                $cnt_company = DB::connection('mysql2')->table('hr_hr')->where('company', 'like', '%'.$company.'%')->get()->toArray();
            }

            return response()->json(['200' => 'ok', 'cnt' => array_merge($cnt_id, $cnt_mail, $cnt_company), 'domain' => $site]);
        } else {
            return response()->json(['error' => 'no id or site for search contacts']);
        }
    }

    /**
     * Получаем параметры поиска в таблице resume.hh_hh,
     * результаты возвращаем csv файлом, сразу на скачивание.
     * Внимание! Смотри VerifyCsrfToken и ApiMiddlware
     *
     * matcher.recruitmnent.ru/api/getContForCSV?city=Воронеж&updated=2020-06-28&source=hh.ru
     *
     *   GET запрос на этот адрес: matcher.recruitmnent.ru/api/getContForCSV
     *   Параметры:
     *   city город
     *   updated дата в формате 2020-06-28
     *   source источник
     *
     *   Запрос сразу возвращает CSV файл.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getContForCSV(Request $request)
    {
        function toPlain($src){
            $total = [];
            foreach ($src as $row){
                $total [] = [
                    $row->id,
                    $row->emp_id,
                    $row->company,
                    $row->name,
                    $row->last_name,
                    $row->normal_name,
                    $row->email,
                    $row->phone,
                    $row->city_id,
                    $row->city,
                    $row->position,
                    $row->source,
                    $row->common_mail,
                    $row->site,
                    $row->updated_at
                ];
            }
            return $total;
        }
        $cols =
            [
                'id',
                'id Компании',
                'Компания',
                'Имя',
                'Фамилия',
                'Нормализованное имя',
                'Почта',
                'Телефон',
                'id Города',
                'Город',
                'Должность',
                'Источник',
                'Общая почта',
                'Сайт',
                'Дата',
        ];

        $cnt = [];

//        $source = isset($request->source) ? $request->source : Null;
//        $city = isset($request->city) ? $request->city : Null;
//        $upd = isset($request->updated) ? $request->updated : Null;
//
//        $res = DB::connection('mysql2')->table('hr_hr')
//            ->where('source', 'like', $source)
//            ->where('city', 'like', $city)
//            ->where('updated_at', '=', $upd)->get();
//
//        $cnt = array_merge($cnt, toPlain($res));


//        // По source
//        if (isset($request->source)) {
//            $res = DB::connection('mysql2')->table('hr_hr')->where('source', 'like', $request->source)->get();
//            $cnt = array_merge($cnt, toPlain($res));
//        }
//        // По city
//        if (isset($request->city)) {
//            $res = DB::connection('mysql2')->table('hr_hr')->where('city', 'like', $request->city)->get();
//            $cnt = array_merge($cnt, toPlain($res));
//        }
//        // По updated
//        if (isset($request->updated)) {
//            $res = DB::connection('mysql2')->table('hr_hr')->where('updated_at', '=', $request->updated)->get();
//            $cnt = array_merge($cnt, toPlain($res));
//        }

        $res = DB::connection('mysql2')->table('hr_hr');
        // По source
        if (isset($request->source)) {
            $res = $res->where('source', 'like', $request->source);
        }
        // По city
        if (isset($request->city)) {
            $res = $res->where('city', 'like', $request->city);
        }
        // По updated
        if (isset($request->updated)) {
            $res = $res->where('updated_at', '=', $request->updated);
        }

        $cnt = array_merge($cnt, toPlain($res->get()));


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
//            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $callback = function() use ($cnt, $cols)
        {
            $file = fopen('php://output', 'w');
            fputs($file, chr(0xEF) . chr(0xBB) . chr(0xBF));
            fputcsv($file, $cols, ';');

            foreach($cnt as $c) {
                fputcsv($file, $c, ';');
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
//        return response()->json(['cnt' => $cnt, 'cols' => $cols]);
    }
}
