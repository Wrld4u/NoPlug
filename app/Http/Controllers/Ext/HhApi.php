<?php
/**
 * Created by PhpStorm.
 * User: AdminCM
 * Date: 25.11.2019
 * Time: 18:12
 */

namespace App\Http\Controllers\Ext;

use App\Jobs\FilterSimilar;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Ext\Models\Org as Org;
use App\Http\Controllers\Ext\Models\OrgVacancy as OrgVac;
use App\Http\Controllers\Ext\Models\TotalVacancy as TotalVac;
use App\Http\Controllers\Ext\Models\Metro as Metro;
use App\Events\GetOrgStat;
use App\Events\GetVacStat;
use App\Http\Controllers\Ext\Models\Similar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class HhApi
{

    /**
     *  get*    - получают по api от hh данные и отдают их массивом (в некоторые get включены fill)
     *  fill*   - получают массив и записывают в БД
     * @todo отмечать checked только в allVacancies, чтобы не проверять уже проверенные
     */


    /**
     * Получаем через api список ссылок на организации
     * и извлекаем из актуальных ссылок id организации
     *
     * и сохраняем в базу
     *
     * неактуальные в массиве $res["noData"]
     *
     * @param $urls
     * @return array
     */
    function getOrgList($urls) {
          $ch = curl_init();
          $res = [];
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
          foreach ($urls as $url) {
              $url = explode('/', $url);
              $vacId = array_pop($url);
//              curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies/'.$vacId); //если на входе ссылки на вакансии, а не компании
              curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/employers/'.$vacId);
              $respond = curl_exec($ch);
              $respond = json_decode($respond, true);

              if (isset($respond['id']) && isset($respond['name'])) {
                  $res['ok'][$respond['id']] = $respond['name'];
              } else {
                  $res["noData"][] = $vacId;
              }

          }
          curl_close($ch);
          //Save to Org table id => name
          $this->fillOrgBase($res['ok']);

          return $res;
      }

    /**
     * Сохраняем, полученные из url вакансий, организации в БД
     * $data - результат getOrgList -> $res["ok"]
     * @param $data
     */
    function fillOrgBase($data) {
          if (!empty($data)) {
              foreach ($data as $key => $val) {
                  $org = Org::firstOrCreate(['org_id' => $key], ['name' => $val]);
              }

              //Убираем все вакансии добавленных организаций из списка общих вакансий
              DB::update('UPDATE total_vacancies SET checked = 1 WHERE parent_id IN (SELECT org_id FROM orgs)');
          }
      }

    /**
     * Получает список коротких вакансий одной организации ($id - организации)
     * за 7 дней, МСК+СПб, 100 вакансий максимум
     *
     * и сохраняем в базу
     *
     * @param $id
     * @param int $period
     * @return mixed
     */
    function getShortVacancies($id, $period = 7) {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
//          curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?employer_id='.$id.'&period='.$period.'&area=1&area=2&per_page=100&page=0');
          curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?employer_id='.$id.'&period='.$period.'&per_page=100&page=0');
          $respond = curl_exec($ch);
          $res = json_decode($respond, true);

//            Log::channel('dev')->info($res);
            usleep(rand(300000, 500000));
            if (!empty($res['items'])) {
                $this->fillShortVacancies($res['items'], $id);
            } else {
            	curl_close($ch);
            	return $res;
            }

            if (array_key_exists('pages', $res) && $res['pages'] > 1) {
                for ($i = 1; $i < $res['pages']; $i++) {
//                    curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?employer_id='.$id.'&period=7&area=1&area=2&per_page=100&page='.$i);
                    curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?employer_id='.$id.'&period='.$period.'&per_page=100&page='.$i);
                    $respond = curl_exec($ch);
                    $res = json_decode($respond, true);

//                    usleep(rand(300000, 500000));
                    if (!empty($res['items'])) {
                        $this->fillShortVacancies($res['items'], $id);
                    }
                }
            }


          curl_close($ch);
//          $this->fillShortVacancies($res['items'], $id);
          return $res;
      }

    /**
     * Записываем набор коротких вакансий ($data - результат getShortVacancies -> $res['items'],
     * $orgId - id организации чьи вакансии) в БД
     * @param $data
     * @param $orgId
     */
    function fillShortVacancies($data, $orgId) {
    //      protected $fillable = ['parent_id', 'vacancy_id', 'name', 'metro_id', 'metro', 'city_id', 'city', 's_text', 'text', 'mod_text', 'checked'];
          if (!empty($data)) {
    //          dd($data);
              foreach ($data as $item) {
                  $modTxt = normalize_np(strip_tags($item['snippet']['requirement'].' '.$item['snippet']['responsibility']));

                  $orgVac = OrgVac::firstOrNew(['vacancy_id' => $item['id']]);
                  $orgVac->parent_id = $orgId;
                  $orgVac->vacancy_id = $item['id'];
                  $orgVac->name = $item['name'];
                  $orgVac->metro_id = isset($item['address']['metro']['station_id']) ? $item['address']['metro']['station_id'] : 0;
                  $orgVac->metro = isset($item['address']['metro']['station_name']) ? $item['address']['metro']['station_name'] : '';
                  $orgVac->city_id = $item['area']['id'];
                  $orgVac->city = $item['area']['name'];
                  $orgVac->url = $item['alternate_url'];
                  $orgVac->s_text = '';//$modTxt;
                  $orgVac->text = strip_tags($item['snippet']['requirement'].' '.$item['snippet']['responsibility']);
                  $orgVac->mod_text = $modTxt;
//                  $orgVac->updated_at = time();
                  $orgVac->created_at = explode('T', $item['created_at'])[0];
                  $orgVac->updated_at = explode('T', $item['published_at'])[0];

                  $orgVac->save();
              }
          }
      }

    /**
     * На входе список id организаций
     * получаем для каждой список коротких вакансий и записываем в БД
     *
     * и сохраняем в базу
     *
     * @param $orgsIds
     */
    function fillAllShortVacancies($orgsIds) {
        if (!empty($orgsIds)) {
//            OrgVac::query()->truncate();

            foreach ($orgsIds as $key => $val) {
                GetOrgStat::dispatch($val);
                $this->getShortVacancies($key);
            }
        }
     }

    /**
     * Получение полной вакансии по её id
     * @param $id
     * @return mixed
     */
    function getFullVacancy($id) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
        curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies/'.$id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $respond = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($respond, true);

        return $res;
    }

    /**
     * Получение справочника по всем странам(города страны)
     * или конкретной стране, указав её id (получим её города)
     * @param int $id
     * @return mixed
     */
    function getAreas($id = -1) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
      if ($id == -1) {
          curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/areas/');
      } else {
          curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/areas/'.$id);
      }
      $respond = curl_exec($ch);
      curl_close($ch);

      $res = json_decode($respond, true);

      return $res;
    }

    /**
     * Получение справочника по всем городам, их метро(линии и станции)
     * или конкретному городу, указав его id (получим линии и станции города)
     * MSK $id = 1, SPb $id = 2;
     * @param int $id
     * @return mixed
     */
    function getMetro($id = -1) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        if ($id == -1) {
            curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/metro/');
        } else {
            curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/metro/'.$id);
        }
        $respond = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($respond, true);

        return $res;
    }

    /**
     * Заполняем таблицу метро по городу $id
     * MSK = 1, SPb = 2
     */
    function fillMetro($id) {
        $res = $this->getMetro($id);
        if (isset($res) && !empty($res['lines'])) {
            $city = $res['name'];
            foreach ($res['lines'] as $line) {
                foreach ($line['stations'] as $station) {
//                    protected $fillable = ['station_id', 'station', 'city_id', 'city'];
                    $stat = Metro::firstOrNew(['station_id' => $station['id']], ['city_id' => $id]);
                    $stat->station = $station['name'];
                    $stat->city = $city;
                    $stat->save();
                }
            }
        }
    }

    /**
     * Получение коротких вакансий (С КОТОРЫМИ будем сравнивать вакансии наших организаций)
     * Данные станций метро берутся из БД
     *
     * $metros -> [ 'city_id' => $city_id, 'station_id' => $station_id ]
     * curl: https://api.hh.ru/vacancies?period=29&area={$city_id}&metro={$station_id}&per_page=100&page=0
     *
     *   "items" => array:100 [▶]
     *   "found" => 158884
     *   "pages" => 20
     *   "per_page" => 100
     *   "page" => 0
     *   "clusters" => null
     *   "arguments" => null
     *   "alternate_url" => "https://hh.ru/search/vacancy?area=1&area=2&enable_snippets=true&items_on_page=100&search_period=29"
     *
     * и сохраняем в базу
     *
     *
     * Нам же нужно раздробить вакансии со станцией метро - загружая каждый день маленькие кусочки вакансий за день
     * + дробя эти маленькие кусочки применяю фильтры по Опыту работу / Типу занятости / Графику работы.
     *
     *
     * @param array $metros
     * @param int $period
     * @return mixed
     */
    function getAllVacancies($metros = [], $period = 30) {
//        TotalVac::query()->truncate();

        if (!empty($metros)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

//            $cm = count($metros);
//            $i = 0;

            foreach ($metros as $ms) {
//                $i++;
//                GetOrgStat::dispatch(round($i/$cm*100, 2));
//                GetOrgStat::dispatch($ms['station']); //==============================================================
                  GetVacStat::dispatch(['id' => $ms['id'], 'name' => $ms['station'], 'more' => 0]);

                curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?period='.$period.'&area='.$ms['city_id'].'&metro='.$ms['station_id'].'&per_page=100&page=0');
//                curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?period='.$period.'&area=1&area=2&metro='.$ms['station_id'].'&per_page=100&page=0');
                $respond = curl_exec($ch);
                $res = json_decode($respond, true);

                if (!empty($res['items'])) {
                    $this->fillAllVacancies($res['items']);
                }

                if ($res['pages'] > 1) {
                    GetVacStat::dispatch(['id' => $ms['id'], 'name' => $ms['station'], 'more' => $res['pages']]);
                    for ($i = 1; $i < $res['pages']; $i++) {
                        curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?period='.$period.'&area='.$ms['city_id'].'&metro='.$ms['station_id'].'&per_page=100&page='.$i);
//                        curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?period='.$period.'&area=1&area=2&metro='.$ms['station_id'].'&per_page=100&page='.$i);
                        $respond = curl_exec($ch);
                        $res = json_decode($respond, true);

                        if (!empty($res['items'])) {
                            $this->fillAllVacancies($res['items']);
                        }
                    }
                }
            }
            curl_close($ch);
        }
    }

    /**
     * Записываем набор коротких вакансий (С КОТОРЫМИ будем сравнивать вакансии наших организаций)
     * @param $data
     */
    function fillAllVacancies($data) {
        //      protected $fillable = ['parent_id', 'vacancy_id', 'name', 'metro_id', 'metro', 'city_id', 'city', 's_text', 'text', 'mod_text', 'checked'];
        if (!empty($data)) {
            //          dd($data);
            foreach ($data as $item) {
                $modTxt = normalize_np(strip_tags($item['snippet']['requirement'].' '.$item['snippet']['responsibility']));

                $totalVac = TotalVac::firstOrNew(['vacancy_id' => $item['id']]);
                $totalVac->parent_id = isset($item['employer']['id']) ? $item['employer']['id'] : 0;
                $totalVac->vacancy_id = $item['id'];
                $totalVac->name = $item['name'];
                $totalVac->metro_id = isset($item['address']['metro']['station_id']) ? $item['address']['metro']['station_id'] : 0;
                $totalVac->metro = isset($item['address']['metro']['station_name']) ? $item['address']['metro']['station_name'] : '';
                $totalVac->city_id = $item['area']['id'];
                $totalVac->city = $item['area']['name'];
                $totalVac->url = $item['alternate_url'];
                $totalVac->s_text = '';//$modTxt;
                $totalVac->text = strip_tags($item['snippet']['requirement'].' '.$item['snippet']['responsibility']);
                $totalVac->mod_text = $modTxt;
                $totalVac->updated_at = time();

                if (isset($item['employer']['id']) && Org::where(['org_id' => $item['employer']['id']])->count() > 0) $totalVac->checked = 1;

                $totalVac->save();
            }
        }
    }

    /**
     * Проверяем наличие Job в Очереди (таблице job)
     * Поиск по имени Job (работы) Ищем по всей очереди workers работу с именем $jobName
     * @param $jobName
     * @return bool
     */
    function checkJob($jobName) {
        $queue = DB::table(config('queue.connections.database.table'))->where('queue', '=', 'workers')->get('payload');
        if($queue){
            foreach ($queue as $job){
                $payload = json_decode($job->payload,true);
                if($payload['displayName'] == $jobName){
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }


    /**
     * Проверяем наличие Job в Очереди (таблице job)
     * Поиск по имени queue (очереди) Если есть хоть одно задание в конкретной очереди
     * @param $queue
     * @return bool
     */
    function checkJobInQueue($queue) {
        $res = DB::table(config('queue.connections.database.table'))->where('queue', '=', $queue)->count();
        if($res > 0){
                return true;
            }
            else {
                return false;
            }
    }

    public function getEmpSite($id)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/employers/'.$id);
        $respond = curl_exec($ch);
        $respond = json_decode($respond, true);
        curl_close($ch);

        if (isset($respond['site_url'])) {
            return $respond['site_url'];
        } else {
            return Null;
        }

    }

    /**
     * Поиск по тексту через api ррр
     * по всем вакансиям
     * @param bool $wCity
     */
    function searchAllByAPI($wCity = false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('User-Agent: api-test-agent'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

//        $orgVacs = OrgVac::whereDate('updated_at', '>=', Carbon::now()->subWeeks(1))->get(['vacancy_id', 'name', 'url', 'parent_id', 'text', 'city_id', 'mod_text'])->toArray();
        $orgVacs = OrgVac::whereDate('updated_at', '>=', Carbon::now()->subDays(2))->get(['vacancy_id', 'name', 'url', 'parent_id', 'text', 'city_id', 'mod_text'])->toArray();
        $total = count($orgVacs);
        $orgs = Org::all('org_id')->pluck('org_id');

        $cur = 0;
        foreach ($orgVacs as $orgVac){
            if ($total > 0) {
                GetOrgStat::dispatch(round(($cur*100/$total), 2));
            }

            if ( $wCity ) {
                curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?area='.$orgVac['city_id'].'&text='.urlencode($orgVac['text']).'&per_page=100&page=0');
            } else {
                curl_setopt($ch, CURLOPT_URL, 'https://api.hh.ru/vacancies?text='.urlencode($orgVac['text']).'&per_page=100&page=0');
            }
            $respond = curl_exec($ch);
            $res = json_decode($respond, true);

//            Log::channel('dev')->info($res);

            if (!empty($res['items'])) {
//                $this->fillAllVacancies($res['items']);
                foreach ($res['items'] as $item) {
//                    $r = in_array((int)$item['employer']['id'], $orgs->toArray()) ? '1' : '0';
//                    return (['res' => $r, 'item id' => (int)$item['employer']['id'], 'orgs' => $orgs->toArray()]);
                    if (!isset($item['employer']['id'])) {
                        $item['employer']['id'] = 0;
                    }
                    if (!in_array((int)$item['employer']['id'], $orgs->toArray())) {
                        $modTxt = normalize_np(strip_tags($item['snippet']['requirement'].' '.$item['snippet']['responsibility']));
//                    if ($item['id'] != $orgVac['vacancy_id']) {
                        // here get employer site_url and employer id and save to Similar
                        $emp = $this->getEmpSite($item['employer']['id']);
                        //save to similar
                        $sim = Similar::firstOrNew(['parent_vacancy_id' => $orgVac['vacancy_id'], 'vacancy_id' => $item['id']]);
                        $sim->name = $item['name'];
                        $sim->url = $item['alternate_url'];
                        $sim->text = strip_tags($item['snippet']['requirement'].' '.$item['snippet']['responsibility']);
                        $sim->mod_text = $modTxt;
                        $sim->emp_site = $emp;
                        $sim->emp_id = $item['employer']['id'];
                        $sim->created_at = explode('T', $item['created_at'])[0];
                        $sim->updated_at = explode('T', $item['published_at'])[0];

                        $sim->save();
                    }
                }
            }

            $cur++;
//            sleep(rand(10,20));
            sleep(1);
        }

        curl_close($ch);

        // Фильтрация полученных похожих вакансий (отсеиваем менее похожие)
        foreach ($orgVacs as $mVac) {
            FilterSimilar::dispatch($mVac)->onQueue('compare');
        }
    }

}

