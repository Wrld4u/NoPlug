<?php

namespace App\Http\Controllers\Ext\Models;

use Illuminate\Database\Eloquent\Model;

class Org extends Model
{
    //allowed for write fields
    protected $fillable = ['org_id', 'name'];
    //set table name
//    protected $table = 'orgs';

    public function vacancy() {
        return $this->hasMany('App\Http\Controllers\Ext\Models\OrgVacancy','parent_id', 'org_id');
    }
}
