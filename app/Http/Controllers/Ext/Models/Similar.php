<?php

namespace App\Http\Controllers\Ext\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Similar extends Model
{

    /**
     * Сравниваем
     *
     *   vvvvvvvv - Org Vacancy id
     *   34275626 => array:4 [▼
     *       0 => array:6 [▼
     *            "vacancy_id" => 27186324
     *            "name" => "Продавец-консультант Juicy Couture"
     *            "url" => "https://hh.ru/vacancy/27186324"
     *            "mod_text" => "жлтлнспшнпрджхктвнжзнннпзцмнрбттькмнджлндстгтьрзльттстнвлвтьсдстгнтмпрдствлнклнтскгсрвсклссстндртмкмпнрзвтдлгсрчнхтншнклнтмсщствлнвклдкпрдкцкмпнмгзн"
     *            "text" => "Желателен успешный опыт в продажах. Активная жизненная позиция. Умение работать в команде. Желание достигать результата и не останавливаться на достигнутом. Предоставление клиентского сервиса класса люкс по стандартам компании. Развитие долгосрочных отношений с клиентами. Осуществление выкладки продукции компании в магазине.  ◀"
     *            "main text" => "Желателен успешный опыт в продажах. Активная жизненная позиция. Умение работать в команде. Желание достигать результата и не останавливаться на достигнутом. Предоставление клиентского сервиса класса люкс по стандартам компании. Развитие долгосрочных отношений с клиентами. Осуществление выкладки продукции компании в магазине.  ◀"
     *            ]
     *       1 => array:6 [▶]
     *       2 => array:6 [▶]
     *       3 => array:6 [▶]
     *       ]
     *   31864045 => array:4 [▶]
     */

    //allowed for write fields
    protected $fillable = ['parent_vacancy_id', 'vacancy_id', 'emp_id', 'emp_site', 'name', 'url', 'text', 'mod_text', 'checked', 'created_at', 'updated_at'];

    public function parent() {
        // return $this->belongsTo('App\Http\Controllers\Ext\Models\OrgVacancy', 'parent_vacancy_id', 'vacancy_id');
        return $this->belongsTo('App\Http\Controllers\Ext\Models\OrgVacancy', 'parent_vacancy_id', 'vacancy_id')->whereDate('updated_at', '>=', Carbon::now()->subDays(3))->get();
    }

}
