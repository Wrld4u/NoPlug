<?php

namespace App\Http\Controllers\Ext\Models;

use Illuminate\Database\Eloquent\Model;

class TotalVacancy extends Model
{
    //allowed for write fields
    protected $fillable = ['parent_id', 'vacancy_id', 'name', 'metro_id', 'metro', 'city_id', 'city', 'url', 's_text', 'text', 'mod_text', 'checked'];
}
