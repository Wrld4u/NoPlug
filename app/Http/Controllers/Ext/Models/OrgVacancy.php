<?php

namespace App\Http\Controllers\Ext\Models;

use Illuminate\Database\Eloquent\Model;
// use Carbon\Carbon;

class OrgVacancy extends Model
{
    //allowed for write fields
    protected $fillable = ['parent_id', 'vacancy_id', 'name', 'metro_id', 'metro', 'city_id', 'city', 'url', 's_text', 'text', 'mod_text', 'checked'];
    //set table name
//    protected $table = 'org_vacancies';

    public function org() {
        // return $this->belongsTo('Org', 'parent_id', 'org_id')->with('sim');
        return $this->belongsTo('Org', 'parent_id', 'org_id');
    }

    public function sim() {
        return $this->hasMany('App\Http\Controllers\Ext\Models\Similar', 'parent_vacancy_id', 'vacancy_id');
        // return $this->hasMany('App\Http\Controllers\Ext\Models\Similar', 'parent_vacancy_id', 'vacancy_id')->whereDate('updated_at', '>=', Carbon::now()->subDays(3));
    }

}
