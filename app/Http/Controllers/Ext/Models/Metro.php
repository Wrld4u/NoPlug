<?php

namespace App\Http\Controllers\Ext\Models;

use Illuminate\Database\Eloquent\Model;

class Metro extends Model
{
    //allowed for write fields
    protected $fillable = ['station_id', 'station', 'city_id', 'city'];

}
