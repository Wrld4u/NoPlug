<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_vacancies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parent_id');
            $table->integer('vacancy_id');
            $table->string('name');
            $table->string('metro_id');
            $table->string('metro');
            $table->integer('city_id');
            $table->string('city');
            $table->string('url');
            $table->longText('s_text');
            $table->longText('text');
            $table->longText('mod_text');
            $table->integer('checked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_vacancies');
    }
}
