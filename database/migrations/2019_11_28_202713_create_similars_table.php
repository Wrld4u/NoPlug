<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSimilarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('similars', function (Blueprint $table) {
//            protected $fillable = ['parent_vacancy_id', 'vacancy_id', 'name', 'url', 'text', 'mod_text', 'checked'];

            $table->bigIncrements('id');
            $table->integer('parent_vacancy_id');
            $table->integer('vacancy_id');
            $table->integer('emp_id')->nullable()->default(Null);
            $table->string('emp_site')->nullable()->default(Null);
            $table->string('name');
            $table->string('url');
            $table->longText('text');
            $table->longText('mod_text');
            $table->integer('checked')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('similars');
    }
}
